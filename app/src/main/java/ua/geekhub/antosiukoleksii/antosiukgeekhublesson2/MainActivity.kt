package ua.geekhub.antosiukoleksii.antosiukgeekhublesson2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout

class MainActivity : AppCompatActivity(), EssenceItemRecyclerAdapter.AdapterCallback {

    private var lastItem: Essence? = null
    private val items = getEssenceList()
    private val listFragment = ListFragment.newInstance(items, this)
    private val detailFragment = DetailFragment()

    override fun onItemClick(item: Essence) {
        lastItem = item
        val bundle = Bundle()
        bundle.putParcelable(ITEM_DATA_KEY, lastItem)
        detailFragment.arguments = bundle;

        val frame: FrameLayout? = findViewById(R.id.detail_frame)
        if (frame == null)
           setInSameFrame()
        else if (item != null)
            setInNewFrame(bundle)
        else
            setInNewFrame(null)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(ITEM_DATA_KEY, lastItem)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        if (savedInstanceState.containsKey(ITEM_DATA_KEY))
            lastItem = savedInstanceState.getParcelable(ITEM_DATA_KEY)

        onItemClick(lastItem!!)
    }

    private fun setInSameFrame() {
        val fm = supportFragmentManager.beginTransaction()
        fm.replace(R.id.frame,  detailFragment)
        fm.addToBackStack(null)
        fm.commit()
    }

    private fun setInNewFrame(bundle: Bundle?) {
        val fm = supportFragmentManager.beginTransaction()
        fm.replace(R.id.detail_frame,  detailFragment)
        fm.commit()
        detailFragment.setValues(bundle)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fm = supportFragmentManager.beginTransaction()
        fm.replace(R.id.frame,  listFragment)
        fm.commit()

        val frame: FrameLayout? = findViewById(R.id.detail_frame)
        if (frame != null) setInNewFrame(null)
    }

    fun openAdditionalTasksActivity(view: View) {
        val intent = Intent(this, AdditionalActivity::class.java)
        startActivity(intent)
    }
}
