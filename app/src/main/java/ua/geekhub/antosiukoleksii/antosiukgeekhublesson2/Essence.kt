package ua.geekhub.antosiukoleksii.antosiukgeekhublesson2

import android.os.Parcel
import android.os.Parcelable


data class Essence (val name: String,
                    val description: String,
                    val img: String) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeString(img)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Essence> {
        override fun createFromParcel(parcel: Parcel): Essence {
            return Essence(parcel)
        }

        override fun newArray(size: Int): Array<Essence?> {
            return arrayOfNulls(size)
        }
    }
}

fun getEssenceList(): List<Essence> {
    val items = mutableListOf<Essence>()
    for (i in 1..25) {
        items.add(Essence("name" + i.toString(),
            "description" + i.toString(),
            "_" + i.toString()))
    }
    return items;
}