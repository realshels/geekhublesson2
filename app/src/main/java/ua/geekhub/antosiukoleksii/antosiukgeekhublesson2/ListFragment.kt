package ua.geekhub.antosiukoleksii.antosiukgeekhublesson2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.list_fragment.view.*


class ListFragment : Fragment() {

    private val adapter = EssenceItemRecyclerAdapter()

    companion object {
        fun newInstance(items: List<Essence>, callback: EssenceItemRecyclerAdapter.AdapterCallback): ListFragment {
            val listFragment = ListFragment()
            listFragment.setCallback(callback)
            listFragment.setItems(items)
            return listFragment
        }
    }

    fun setItems(items: List<Essence>) = adapter.setNewItems(items)

    fun setCallback(callback: EssenceItemRecyclerAdapter.AdapterCallback) = adapter.setCallback(callback)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.list_fragment, container, false)
        view.items_view.adapter = adapter
        return view
    }

}
