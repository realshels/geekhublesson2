package ua.geekhub.antosiukoleksii.antosiukgeekhublesson2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_detail.view.*

class DetailFragment : Fragment() {

    private fun innerSetValues(view: View?, bundle: Bundle?) {
        val item = bundle?.getParcelable<Essence>(ITEM_DATA_KEY)

        view?.txtDetailName?.text = item?.name ?: R.string.no_data.toString()

        view?.txtDetailDescription?.text = item?.description ?: R.string.no_data.toString()

        if (item != null) {
            val id = view?.context?.getResources()?.getIdentifier(
                item.img,
                "drawable",
                view?.context?.applicationInfo?.packageName
            )
            view?.imgLogo?.setImageResource(id ?: 0)
        }
    }

    fun setValues(args: Bundle?) {
        innerSetValues(this.view, args)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_detail, container, false)
        val args = this.arguments
        innerSetValues(v, args)
        return v
    }
}
