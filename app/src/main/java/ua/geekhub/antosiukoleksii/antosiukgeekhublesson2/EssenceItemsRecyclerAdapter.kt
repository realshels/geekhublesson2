package ua.geekhub.antosiukoleksii.antosiukgeekhublesson2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.view_item.view.*

class EssenceItemRecyclerAdapter : RecyclerView.Adapter<EssenceItemRecyclerAdapter.ViewHolder>() {
    private val items = mutableListOf<Essence>()

    interface AdapterCallback {
        fun onItemClick(item: Essence)
    }

    private var callback: AdapterCallback? = null

    fun setCallback(callback: AdapterCallback) {
        this.callback = callback
    }

    fun setNewItems (items: List<Essence>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int  = items.count()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
         holder.onBind(items[position])
    }

    inner class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        private var item: Essence? = null

        init {
            view.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            callback?.onItemClick(this.item!!)
        }

        fun onBind(item: Essence) {
            view.txtName.text = item.name
            this.item = item
            val id = view.context.getResources().getIdentifier(
                item.img,
                "drawable",
                view.context.applicationInfo.packageName
            )
           view.imageView.setImageResource(id)
        }
    }
}